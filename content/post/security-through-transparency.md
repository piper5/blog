+++
title = "Security through Transparency"
date = "2023-02-12"
author = "Piper"
description = "Or why us having nothing to hide means you have nothing to worry about"
tags = ["security", "transparency", "philosophy"]
+++
## Security through Obscurity

_To keep your secret is wisdom; but to expect others to keep it is folly. - Samuel Johnson_

Security through obscurity is the practice of relying on secrecy to provide security to a system or component. You've probably heard this phrase before, and probably with some negative connotations attached. 

And rightly so, given the propensity of data breaches, hijacked logins, online impersonation, and identity theft, it's no wonder the idea of secrecy carries some cynicism with it these day. 

Data breaches agaist institutions and services we've come to trust and rely on have been jump-off points for online impersonators and identity thieves, which cost consumers tens of billions of dollars each year. 

On top of the impacts of identity theft and fraud on consumers, is that of ransomware attacks against providers, the prevention of which can result in increased complexity and costs for data security, backup, and disaster recovery. 

These costs are harmful to a business's bottom line, but ultimately get passed on to the consumer, who takes the lions share of the burden.

## Security through Transparency

_Secrecy was the problem; transparency the obvious cure. - Robert J. Sawyer_

WaxJack fully embraces security through transparency. We do this in several ways:

First, by not storing any shared secrets, such as passwords, there are no user credentials to compromise. In fact, we could function without storing any user information at all besides a public key id. However doing so allows us to offer additional features with much less effort. 

Second, we don't store any personal identifable information in private. You are free to associate your account with other online identities you control, such as email addresses and social media accounts. However, these will be publicly viewable to the world. Each online identity you add to your account is further evidence that you are who you claim to be, and not an imposter. (N.B. Among Us fans)

Third, by decoupling configuration from code. Our codebase will always be 100% public, opensource, free as in freedom and free as in beer. Even our infrastructure code. 

Fourth, by keeping all issues, bugs, roadmaps, and changelogs public, you get to see all of our bad hair days. No filters. 

Finally, by letting go of our need to stave off competition. 

While we charge a nominal fee for premium features, these features will never be kept locked away in a private repository. Anyone can pony up the money for a cloud account, run our infrastructure provisioning code, and push a deployment, and run their own waxjack service. In fact, we would love it if you did! Just please note the license agreements and if you want to send us a message, we'd love to see what you've done with the place. 

So why are we so willing to be so transparent? Because reputation is our business, literally. Transparency is a prerequisite for accountability, and without accountability how can there be trust? You shouldn't have to worry if you can trust us with your secrets. We're not keeping any.

The best part is that we don't have to pay for the overhead of protecting private data, we don't need to pay for the costs of compliance with data privacy regulations, we don't have to pay for liability insurance against data breaches, because we have no private data to breach. This all translates into cost savings for consumers, and a time savings for us so we can focus our energy on providing you with great performance and features. 